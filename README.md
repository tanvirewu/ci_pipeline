# CI Pipeline for Django

### About

A continuous integration pipeline that is triggered when code is pushed to the `feature` branch.

The pipeline runs on a private Gitlab runner with a `shell` executor. The runner has `docker` and `docker-compose` installations and is listening at port 8000.

#### Pipeline Stages:

1. `test`: Runs the tests of the app using `pytest`
2. `build`: Launches the app in a container for review.


#### The Gitlab Runner:

Configuring an Ubuntu 20.04 instance:

```
## system updates and Docker installation
sudo apt update
sudo apt upgrade -y
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt install docker-ce -y

## test run Docker
sudo usermod -aG docker ubuntu
su - ubuntu
docker run hello-world

## Docker Compose installation
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version

## installing the gitlab-runner package
sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
sudo gitlab-runner register
## respond to prompt..

sudo gitlab-runner status
sudo usermod -aG docker gitlab-runner
sudo gitlab-runner restart
```

**Troubleshooting:**

If the pipeline immediately fails to run on the runner, clean up and then rerun it:

```
ls -la /home/gitlab-runner
rm -rf /home/gitlab-runner/.bash_logout 
```
